provider "aws" {
    region = "ap-south-1"
}

variable "cidr_blocks" {
  description = "subnet cidr block"
  type = list(string)
}

variable "AZ" {}

resource "aws_vpc" "nayak_vpc" {
  cidr_block = var.cidr_blocks[0]
  tags = {
    Name: "dev_vpc"
  }
}
resource "aws_subnet" "nayak_subnet-1" {
  vpc_id     = aws_vpc.nayak_vpc.id
  cidr_block = var.cidr_blocks[1]
  availability_zone = var.AZ
  tags = {
    Name: "dev_sub-1"
  }
}
