# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/aws" {
  version     = "5.34.0"
  constraints = "5.34.0"
  hashes = [
    "h1:1Y1JgV1z99QqAK06+atyfNqreZxyGZKbm4mZO4VhhT8=",
    "zh:01bb20ae12b8c66f0cacec4f417a5d6741f018009f3a66077008e67cce127aa4",
    "zh:3b0c9bdbbf846beef2c9573fc27898ceb71b69cf9d2f4b1dd2d0c2b539eab114",
    "zh:5226ecb9c21c2f6fbf1d662ac82459ffcd4ad058a9ea9c6200750a21a80ca009",
    "zh:6021b905d9b3cd3d7892eb04d405c6fa20112718de1d6ef7b9f1db0b0c97721a",
    "zh:9b12af85486a96aedd8d7984b0ff811a4b42e3d88dad1a3fb4c0b580d04fa425",
    "zh:9e61b8e0ccf923979cd2dc1f1140dbcb02f92248578e10c1996f560b6306317c",
    "zh:ad6bf62cdcf531f2f92f6416822918b7ba2af298e4a0065c6baf44991fda982d",
    "zh:b698b041ef38837753bbe5265dddbc70b76e8b8b34c5c10876e6aab0eb5eaf63",
    "zh:bb799843c534f6a3f072a99d93a3b53ff97c58a96742be15518adf8127706784",
    "zh:cebee0d942c37cd3b21e9050457cceb26d0a6ea886b855dab64bb67d78f863d1",
    "zh:e061fdd1cb99e7c81fb4485b41ae000c6792d38f73f9f50aed0d3d5c2ce6dcfb",
    "zh:eeb4943f82734946362696928336357cd1d36164907ae5905da0316a67e275e1",
    "zh:ef09b6ad475efa9300327a30cbbe4373d817261c8e41e5b7391750b16ef4547d",
    "zh:f01aab3881cd90b3f56da7c2a75f83da37fd03cc615fc5600a44056a7e0f9af7",
    "zh:fcd0f724ebc4b56a499eb6c0fc602de609af18a0d578befa2f7a8df155c55550",
  ]
}

provider "registry.terraform.io/linode/linode" {
  version     = "2.13.0"
  constraints = "2.13.0"
  hashes = [
    "h1:uB4/YAJHUhSloaAyB+CNgeu0DPstQL8id+Pmq0L/OK4=",
    "zh:25776adaba8c01251f02326b356e960f5e7ea1bf5f7e60b3b598792a0a3b5b4f",
    "zh:30bc848498bcf2ad1366ea32068c2983d982140669aa507bb7a21714fe5f6beb",
    "zh:32b5078f3defe920d611da19ff53139cbdff7de74a19238a9611d206255d5eb8",
    "zh:32b69e72ed06273550888b5fa6c53b05f37d6fb42aa898b1981e0e40d5332cd3",
    "zh:7b1df46d734b461b006f2fc92a8f8e4e810afa5458c50f2016ee99e1541f6a4b",
    "zh:7eda947ae4aefd486e758d6f86985607c9764ea55556aacf8a9fcc78780fa6d0",
    "zh:832ec3adf887bcbbff99021ca1518e44f51e1c6af0d7fe639ceddc92921df130",
    "zh:8cd1dfd9edcdd9432ce567981dc653cc2cdedf6349513614493c37485533d519",
    "zh:a5cf20563230d2180fd48a1716315f7ccfb20d8e12eceb29609135122a2a07db",
    "zh:b8d66ae8c6fbd31cea8cfc15f8160e3e00b6d79f8afcdd2b88ccbe63c3bbc34e",
    "zh:da0a28bde0bf5ac818fc07c7ab1136e7dfb46efa98fd38e1450104582d629f96",
    "zh:dc611263577f1ee319a229fb2e47d1ab5ad99cfab8eb4d43c09afcafefb00f1d",
    "zh:e23355243d5c024caec26f072035b67707f09ee1231361168919023d2ca15c65",
    "zh:e51d4813a58e79bc953c77db295e23a8abf0f8c21afcfc6401dc1010b9613a48",
  ]
}
